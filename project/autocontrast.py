from __future__ import print_function
import argparse
import numpy as np
import matplotlib.image as mpimg


def count_black_qv (hist, sum_pix_for_qv, max_ind):
    sum_ = 0
    low_qv = 0
    while sum_ < sum_pix_for_qv and low_qv <= max_ind:
        sum_ = sum_ + hist[low_qv]
        low_qv += 1
    if low_qv != 0:
        low_qv -=1
    if sum_ > sum_pix_for_qv and low_qv != 0:
        low_qv -= 1
    return low_qv


def count_white_qv(hist, sum_pix_for_qv, min_ind):
    sum_ = 0
    high_qv = len(hist) - 1
    while sum_ < sum_pix_for_qv and high_qv >= min_ind:
        sum_ = sum_ + hist[high_qv]
        high_qv -= 1
    if high_qv != len(hist) - 1:
        high_qv +=1
    if sum_ > sum_pix_for_qv and high_qv != len(hist) - 1:
        high_qv += 1
    return high_qv


def auto_contrast_1ch(src_im_, black_sum, white_sum):
    dst_im = np.array(src_im_, dtype=np.int)
    hist_array, bins = np.histogram(src_im_, range=(0, 255), bins=256)
    low_qv = count_black_qv(hist_array, black_sum, 255)
    high_qv = count_white_qv(hist_array, white_sum, 0)
    dst_im =  np.maximum(np.minimum((((dst_im - low_qv) * 255.)/(high_qv - low_qv)),255),0)
    return dst_im


def auto_contrast(src_path, dst_path, white_perc, black_perc):
    src_img = mpimg.imread(src_path)
    dst_img = np.array(src_img, np.int)

    n = src_img.shape[0] * src_img.shape[1]
    white_num = n * white_perc
    black_num = n * black_perc
    for i in range(src_img.shape[2]):
        dst_img[:, :, i] = auto_contrast_1ch(src_img[:, :, i], black_num, white_num)

    mpimg.imsave(dst_path + ".png", dst_img)


if __name__ == '__main__':

    ap = argparse.ArgumentParser()

    ap.add_argument("-s", "--src", required=True,
                    help="Input path to src_im")
    ap.add_argument("-d", "--dst", required=True,
                    help="Output path to dst_im")
    ap.add_argument("-w", '--white_percentage', type = float, required=True,
                    help='White pixels percentage')
    ap.add_argument("-b", '--black_percentage', type = float, required=True,
                    help='Black pixels percentage')

    args = vars(ap.parse_args())

    assert 0 <= args['white_percentage'] < 1
    assert 0 <= args['black_percentage'] < 1

    auto_contrast(args['src'], args['dst'], args['white_percentage'], args['black_percentage'])