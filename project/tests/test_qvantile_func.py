from project.autocontrast import count_black_qv, count_white_qv
import numpy as np

def test_1():
    hist_arr = np.ones(10)
    assert count_black_qv(hist_arr, 0, 9) == 0


def test_2():
    hist_arr = np.ones(10)
    assert count_black_qv(hist_arr, 10, 9) == 9


def test_3():
    hist_arr = np.ones(10)
    assert count_black_qv(hist_arr, 5, 9) == 4


def test_4():
    hist_arr = np.ones(10)
    assert count_white_qv(hist_arr, 0, 0) == 9


def test_5():
    hist_arr = np.ones(10)
    assert count_white_qv(hist_arr, 10, 0) == 0


def test_6():
    hist_arr = np.ones(10)
    assert count_white_qv(hist_arr, 3, 0) == 7


def test_7():
    hist_arr = np.arange(1, 11, 1)
    assert count_black_qv(hist_arr, 5, 9) == 1


def test_8():
    hist_arr = np.arange(1, 11, 1)
    assert count_white_qv(hist_arr, 5, 0) == 9


def test_9():
    hist_arr = np.arange(1, 11, 1)
    assert count_white_qv(hist_arr, 21, 0) == 8