import os
from setuptools import setup

with open("README", "r") as f:
    long_description = f.read()

setup(
    name="image_autocontrast",
    version="0.1",
    author="Panfilova Ekaterina",
    author_email="panfilova@phystech.edu",
    description="Package",
    long_description=long_description,
    packages=['project', 'project/tests'],
    install_requires=['numpy', 'matplotlib', 'pytest'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)